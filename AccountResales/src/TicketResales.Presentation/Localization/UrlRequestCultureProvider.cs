﻿using Microsoft.AspNetCore.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;

namespace AccountResales.Presentation.Localization
{
    public class UrlRequestCultureProvider : IRequestCultureProvider
    {
        private static RequestCulture rCulture;

        public UrlRequestCultureProvider(RequestCulture rCulture)
        {
            UrlRequestCultureProvider.rCulture = rCulture;
        }

        public static string GetDefaultCulture
        {
            get
            {
                return rCulture.Culture.TwoLetterISOLanguageName;
            }
        }


        public Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            PathString url = httpContext.Request.Path;

            if (url.ToString().Length <= 1)
            {
                return Task.FromResult<ProviderCultureResult>(
                    new ProviderCultureResult(
                        rCulture.Culture.TwoLetterISOLanguageName,
                        rCulture.UICulture.TwoLetterISOLanguageName));
            }

            string culture = httpContext.Request.Path.Value.Split('/').GetValue(1).ToString();

            if (!Regex.IsMatch(culture, @"^[a-z]{2}(-[A-Z]{2})*$"))
            {
                return Task.FromResult<ProviderCultureResult>(
                    new ProviderCultureResult(
                        rCulture.Culture.TwoLetterISOLanguageName,
                        rCulture.UICulture.TwoLetterISOLanguageName));
            }

            return Task.FromResult<ProviderCultureResult>(
                new ProviderCultureResult(culture, culture));
        }
    }
}
