﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Business.Models;
using System.Security.Claims;
using Microsoft.Extensions.Localization;
using AccountResales.Presentation.Models.GameViewModels;
using AccountResales.Data.Services;
using AccountResales.Data.Constants;

namespace AccountResales.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private IUserRepository userRep;
        private IAccountRepository accountRep;
        private IGameRepository gameRep;
        private IOrderRepository orderRep;
        private IStringLocalizer<HomeController> loc;
        private GameSortService gameSortService;

        public HomeController(IUserRepository userRep, IAccountRepository accountRep, IGameRepository gameRep,
                              IOrderRepository orderRep, IStringLocalizer<HomeController> loc, GameSortService gameSortService)
        {
            this.userRep = userRep;
            this.accountRep = accountRep;
            this.gameRep = gameRep;
            this.orderRep = orderRep;
            this.loc = loc;
            this.gameSortService = gameSortService;
        }

        public IActionResult Index()
        {
            var games = gameRep.GetAll().Where(x => x.Date.Date >= DateTime.Now.Date);
            List<GameDetailsViewModel> gameDetails = new List<GameDetailsViewModel>();
            games = gameSortService.SortGamesByDate(games).Take(GameConstants.CountGamesForView).ToList();
            games.ToList().ForEach(x => gameDetails.Add(new GameDetailsViewModel() { Game = x, Accounts = accountRep.GetAllByGameId(x.Id).ToList() }));
            return View(gameDetails);
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Details(int idGame)
        {
            Game e = gameRep.GetById(idGame);
            List<Account> accountsGame = accountRep.GetAllByGameId(e.Id).ToList();

            GameDetailsViewModel gameDetails = new GameDetailsViewModel() { Game = e, Accounts = accountsGame };
             
            return View(gameDetails);
        }

        [Authorize]
        [HttpPost]
        public IActionResult BoughtAccount(int accountId, string returnUrl)
        {
            try
            {
                Account account = accountRep.GetById(accountId);
                //if (orderRep.GetByAccount(account) != null) // if user bought this account before
                //{
                //    return loc["You have denied the purchase of this account!"];
                //}
                account.Status = StatusAccount.WaitingConfirmation;
                accountRep.Update(account);

                orderRep.Create(new Order()
                {
                    Account = account,
                    Buyer = userRep.GetById(
                    Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value)
                    ),
                    Status = StatusOrder.WaitingConfirmation
                });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home");
            }
            return Redirect(returnUrl);
        }

        [HttpPost]
        public IActionResult ChangeLang(string lang, string returnUrl)
        {
            RouteData.Values["culture"] = lang;
            if (User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier) != null)
            {
                User user = userRep.GetById(
                    Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value)
                    );
                user.Localization = lang;
                userRep.Update(user);
            }
            return Redirect(returnUrl);
        }

    }
}
