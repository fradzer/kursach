﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using AccountResales.Presentation.Models;
using AccountResales.Presentation.Models.AccountViewModels;
using AccountResales.Presentation.Services;
using AccountResales.Business.Models;
using AccountResales.Data.Utils;
using Microsoft.Extensions.Localization;
using AccountResales.Data.Repositories.Interfaces;

namespace AccountResales.Presentation.Controllers
{
    public class UserAccountController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IStringLocalizer<UserAccountController> localizer;
        private readonly IUserRepository userRep;
        

        public UserAccountController(
            UserManager<User> userManager, SignInManager<User> signInManager, IStringLocalizer<UserAccountController> loc,
            IUserRepository userRepository)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userRep = userRepository;
            localizer = loc;
        }

        //
        // GET: /Account/Login
        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                string hashPassword = PasswordUtil.GetSHA1HashString(model.Password);
                var result = await signInManager.PasswordSignInAsync(model.LastName, hashPassword, false, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    User user = userRep.GetUserByLastName(model.LastName);

                    createClaim(user);

                    RouteData.Values["culture"] = user.Localization;
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, localizer["Incorrect login or password!"]);
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [HttpGet]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new User { Email = model.Email, FirstName = model.FirstName, LastName = model.LastName,
                                      Phone = model.Phone, Address = model.Address};
                var result = await userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToLocal(returnUrl);
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Manage()
        {
            User user = userRep.GetById(
                    Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value)
                    );
           
            return View(user);
        }

        [Authorize]
        [HttpPost]
        public string SaveInfo(int userId,/* string email, */string firstName,
                                string lastName, string address, string phone)
        {
            if(/*!string.IsNullOrWhiteSpace(email) && */!string.IsNullOrWhiteSpace(firstName) &&
                !string.IsNullOrWhiteSpace(lastName) && !string.IsNullOrWhiteSpace(address) &&
                !string.IsNullOrWhiteSpace(phone))
            {
                User user = userRep.GetById(
                    Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value)
                    );
                
                //if(!user.Email.Equals(email) && userRep.GetUserByEmail(email) != null)
                //{
                //    return localizer["This email busy!"];
                //}
                //user.Email = email;
                user.FirstName = firstName;
                user.LastName = lastName;
                user.Address = address;
                user.Phone = phone;
                userRep.Update(user);
            }

            return localizer["Success"];
        }

        [Authorize]
        [HttpPost]
        public string ChangePassword(int userId,string oldPassword, string newPassword, string confirmNewPassword)
        {
            User user = userRep.GetById(userId);
            if(!user.PasswordHash.Equals(PasswordUtil.GetSHA1HashString(oldPassword)))
            {
                return localizer["Wrong password!"];
            }
            if(!newPassword.Equals(confirmNewPassword))
            {
                return localizer["Wrong confirm password!"];
            }
            user.PasswordHash = PasswordUtil.GetSHA1HashString(newPassword);
            userRep.Update(user);
            return localizer["Success"];
        }

        //
        // POST: /Account/LogOff
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await signInManager.SignOutAsync();
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }        
        
        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<User> GetCurrentUserAsync()
        {
            return userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        private async void createClaim(User user)
        {
            List<Claim> roleClaim = new List<Claim> {
                                        new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.Name)
                                        };
            ClaimsIdentity id = new ClaimsIdentity(roleClaim, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                                ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }
        #endregion
    }
}
