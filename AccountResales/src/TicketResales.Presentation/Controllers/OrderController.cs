﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Business.Models;
using System.Security.Claims;

namespace AccountResales.Presentation.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private IUserRepository userRep;
        private IOrderRepository orderRep;
        private IAccountRepository accountRep;

        public OrderController(IUserRepository userRep, IOrderRepository orderRep,
                                IAccountRepository accountRep)
        {
            this.userRep = userRep;
            this.orderRep = orderRep;
            this.accountRep = accountRep;
        }
        
        public IActionResult MyOrders()
        {
            User user = userRep.GetById(
                    Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value)
                    ); 
            List<Order> orders = orderRep.GetAllByBuyerId(user.Id).ToList();

            return View(orders);
        }

        [HttpDelete]
        public IActionResult Delete(int orderId)
        {
            try
            {
                Account account = orderRep.GetById(orderId).Account;
                account.Status = StatusAccount.Selling;
                accountRep.Update(account);
                orderRep.Delete(orderId);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error","Home");
            }
            return RedirectToAction("MyOrders");
        }
    }
}
