﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Business.Models;
using System.Security.Claims;
using AccountResales.Presentation.Models.AccountViewModels;

namespace AccountResales.Presentation.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private IUserRepository userRep;
        private IAccountRepository accountRep;
        private IGameRepository gameRep;
        private IOrderRepository orderRep;

        public AccountController(IUserRepository userRep, IAccountRepository accountRep, IGameRepository gameRep,
                                IOrderRepository orderRep)
        {
            this.userRep = userRep;
            this.accountRep = accountRep;
            this.gameRep = gameRep;
            this.orderRep = orderRep;
        }

        public IActionResult MyAccounts()
        {
            User user = userRep.GetById(
                    Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value)
                    );
            List<Account> accounts = accountRep.GetAllByUserId(user.Id).ToList();

            List<Game> games = gameRep.GetAll().ToList();

            AccountViewModel accountVM = new AccountViewModel() { Accounts = accounts, AllGames = games };

            return View(accountVM);
        }

        [HttpPost]
        public IActionResult Create(int gameId, decimal price)
        {
            Game needGame = gameRep.GetById(gameId);
            if (needGame != null && price > 0)
            {
                try
                {
                    Account newAccount = new Account()
                    {
                        Game = needGame,
                        Price = price,
                        Seller = userRep.GetById(
                    Convert.ToInt32(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value)
                    ),
                        Status = StatusAccount.Selling
                    };
                    accountRep.Create(newAccount);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Home");
                }
            }
            return RedirectToAction("MyAccounts");
        }

        [HttpDelete]
        public IActionResult Delete(int accountId)
        {
            Account account = accountRep.GetById(accountId);

            if (account.Status != StatusAccount.Sold)//remove order by this account
            {
                orderRep.DeleteByAccount(account);
            }
            accountRep.Delete(accountId);


            return RedirectToAction("MyAccounts");
        }

        [HttpPost]
        public IActionResult Approve(int accountId, string track)
        {
            try
            {
                Account account = accountRep.GetById(accountId);
                account.Status = StatusAccount.Sold;
                accountRep.Update(account);

                Order order = orderRep.GetByAccount(account);
                order.Status = StatusOrder.Confirmed;
                order.Track = track;
                orderRep.Update(order);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("MyAccounts");
        }

        [HttpPost]
        public IActionResult Reject(int accountId, string comment)
        {
            try
            {
                Account account = accountRep.GetById(accountId);
                account.Status = StatusAccount.Selling;
                accountRep.Update(account);

                Order order = orderRep.GetByAccount(account);
                order.Status = StatusOrder.Rejected;
                order.Comment = comment;
                orderRep.Update(order);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("MyAccounts");
        }
    }
}
