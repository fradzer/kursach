﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Presentation.Models.GameViewModels
{
    public class GameDetailsViewModel
    {
        public Game Game { get; set; }
        public List<Account> Accounts { get; set; }
    }
}
