﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Presentation.Models.AccountViewModels
{
    public class AccountViewModel
    {
        public List<Account> Accounts { get; set; }
        public List<Game> AllGames { get; set; }
    }
}
