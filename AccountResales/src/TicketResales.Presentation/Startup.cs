﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AccountResales.Presentation.Services;
using Microsoft.AspNetCore.Identity;
using AccountResales.Business.Models;
using AccountResales.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using AccountResales.Presentation.Localization;
using Microsoft.Extensions.Options;
using AccountResales.Data.Migrations;
using AccountResales.Data.Repositories.MSSQLRepositories;
using Swashbuckle.AspNetCore.Swagger;
using AccountResales.Data.Services;

namespace AccountResales.Presentation
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets("aspnet-AccountResales.Presentation-0affb233-f856-46d0-a735-e88968aa3d0b");
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var connection = Configuration.GetConnectionString("ResaleDBConnection");
            services.AddDbContext<ResaleDbContext>(options => options.UseSqlServer(connection));

            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddIdentity<User, Role>()
                .AddDefaultTokenProviders();

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc().AddViewLocalization(
                Microsoft.AspNetCore.Mvc.Razor.LanguageViewLocationExpanderFormat.SubFolder).
                AddDataAnnotationsLocalization();

            services.AddScoped<IUserRepository, UserSqlRepository>();
            services.AddScoped<IOrganizationRepository, OrganizationSqlRepository>();
            services.AddScoped<IFounderRepository, FounderSqlRepository>();
            services.AddScoped<IGameRepository, GameSqlRepository>();
            services.AddScoped<IAccountRepository, AccountSqlRepository>();
            services.AddScoped<IRoleRepository, RoleSqlRepository>();
            services.AddScoped<IOrderRepository, OrderSqlRepository>();
            services.AddScoped<IPasswordHasher<User>, PasswordHasher>();
            services.AddScoped<IUserStore<User>, InUserStore>();
            services.AddScoped<IRoleStore<Role>, InRoleStore>();
            services.AddScoped<GameFilterService>();
            services.AddScoped<GameSortService>();

            services.AddScoped<IUserClaimsPrincipalFactory<User>, AppClaimsPrincipalFactory>();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var cultures = new[]
                {
                    new CultureInfo("en"),
                    new CultureInfo("ru"),
                    new CultureInfo("be")
                };
                options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
                options.SupportedCultures = cultures;
                options.SupportedUICultures = cultures;
                options.RequestCultureProviders.Insert(0, new UrlRequestCultureProvider(options.DefaultRequestCulture));
            });
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                c.DescribeAllEnumsAsStrings();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
                              ILoggerFactory loggerFactory)
        {

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();


            app.UseRequestLocalization(
                app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>().Value);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Register");
            }

            app.UseStaticFiles();
            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = "Cookies",
                LoginPath = new Microsoft.AspNetCore.Http.PathString("/en/Account/Login"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = false
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUi(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "adminRoute",
                    template: "{culture}/{area:exists}/{controller=Admin}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{culture=en}/{controller=Home}/{action=Index}/{id?}");

            });
            app.Ititialize();

        }
    }
}
