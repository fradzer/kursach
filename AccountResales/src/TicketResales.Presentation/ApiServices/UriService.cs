﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Presentation.ApiService
{
    public class UriService
    {
        /// <summary>
        /// Adds the specified parameter to the Query String.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="paramName">Name of the parameter to add.</param>
        /// <param name="paramValue">Value for the parameter to add.</param>
        /// <returns>Url with added parameter.</returns>
        public static UriBuilder AddParameter(UriBuilder uriBuilder, string paramName, string paramValue)
        {
            var query = Microsoft.AspNetCore.WebUtilities.QueryHelpers.ParseQuery(uriBuilder.Query);
            query[paramName] = paramValue;
            uriBuilder.Query += ("&"+query.Keys.Last() + "=" + query.Values.Last());

            return uriBuilder;
        }
    }
}
