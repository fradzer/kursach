﻿using AccountResales.Business.Models;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Data.Utils;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Presentation.Services
{
    public class PasswordHasher : IPasswordHasher<User>
    {
        
        public string HashPassword(User user, string password)
        {
            string hashedPassword = PasswordUtil.GetSHA1HashString(password);
           
            return hashedPassword;
        }

        public PasswordVerificationResult VerifyHashedPassword(User user, string hashedPassword, string providedPassword)
        {
            if (hashedPassword == providedPassword)
            {
                return PasswordVerificationResult.Success;
            }
            else
            {
                return PasswordVerificationResult.Failed;
            }
        }
    }
}
