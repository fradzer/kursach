﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Presentation.Areas.Admin.Models.OrganizationViewModels
{
    public class OrganizationViewModel
    {
        public List<Organization> Organizations { get; set; }
        public List<Founder> AllFounders { get; set; }
    }
}
