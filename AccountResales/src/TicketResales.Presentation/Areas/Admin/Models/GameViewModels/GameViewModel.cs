﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Presentation.Areas.Admin.Models.GameViewModels
{
    public class GameViewModel
    {
        public List<Game> Games { get; set; }
        public List<Organization> AllOrganizations { get; set; }
    }
}
