using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Business.Models;
using Microsoft.Extensions.Localization;
using AccountResales.Presentation.Areas.Admin.Models.OrganizationViewModels;

namespace AccountResales.Presentation.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class OrganizationController : Controller
    {
        private IFounderRepository founderRep;
        private IGameRepository gameRep;
        private IOrganizationRepository organizationRep;
        private IStringLocalizer<OrganizationController> loc;

        public OrganizationController(IOrganizationRepository organizationRep, IFounderRepository founderRep, IGameRepository gameRep,
                              IStringLocalizer<OrganizationController> loc)
        {
            this.organizationRep = organizationRep;
            this.founderRep = founderRep;
            this.gameRep = gameRep;
            this.loc = loc;
        }

        public IActionResult Index()
        {
            List<Organization> organizations = organizationRep.GetAll().ToList();
            List<Founder> founders = founderRep.GetAll().ToList();

            OrganizationViewModel organizationVM = new OrganizationViewModel() { Organizations = organizations, AllFounders = founders };


            return View("Organizations", organizationVM);
        }

        [HttpDelete]
        public IActionResult Delete(int organizationId)
        {
            if (gameRep.GetByOrganizationId(organizationId).Count() != 0)
            {
                return RedirectToAction("Error", "Admin");//if this use
            }
            else
            {
                Organization organization = organizationRep.GetById(organizationId);
                if (organization == null)
                {
                    return RedirectToAction("Error", "Admin");
                }
                else
                {
                    organizationRep.Delete(organizationId);
                    return RedirectToAction("Index");
                }
            }
        }

        [HttpPost]
        public IActionResult Create(int founderId, string address, string name)
        {
            try
            {
                if (address.Length < 3 && name.Length < 3)
                {
                    return RedirectToAction("Error", "Admin");
                }
                else
                {
                    Organization organization = new Organization() {Founder = founderRep.GetById(founderId), Address = address, Name = name };
                    organizationRep.Create(organization);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Admin");
            }
        }


        [HttpPost]
        public IActionResult Update(int organizationId, int founderId, string address, string name)
        {
            try
            {
                if (address.Length < 3 && name.Length < 3)
                {
                    return RedirectToAction("Error", "Admin");
                }
                else
                {
                    Organization organization = organizationRep.GetById(organizationId);
                    organization.Founder = founderRep.GetById(founderId);
                    organization.Address = address;
                    organization.Name = name;
                    organizationRep.Update(organization);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Admin");
            }
        }
    }
}