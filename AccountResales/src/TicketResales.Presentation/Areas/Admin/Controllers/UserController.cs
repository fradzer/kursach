using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AccountResales.Business.Models;
using AccountResales.Data.Repositories.Interfaces;

namespace AccountResales.Presentation.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class UserController : Controller
    {
        private IRoleRepository roleRep;
        private IUserRepository userRep;

        public UserController(IUserRepository userRep, IRoleRepository roleRep)
        {
            this.userRep = userRep;
            this.roleRep = roleRep;
        }

        public IActionResult Index()
        {
            List<User> users = userRep.GetAll().ToList();
            return View("Users",users);
        }

        [HttpPost]
        public IActionResult Revoke(int userId)
        {
            User user = userRep.GetById(userId);
            user.Role = roleRep.GetByName("User");
            userRep.Update(user);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Grand(int userId)
        {
            User user = userRep.GetById(userId);
            user.Role = roleRep.GetByName("Admin");
            userRep.Update(user);
            return RedirectToAction("Index");

        }
    }
}