using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Business.Models;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Internal;
using AccountResales.Data.Utils;
using System.IO;
using AccountResales.Presentation.Areas.Admin.Models.GameViewModels;

namespace AccountResales.Presentation.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class GameController : Controller
    {
        private IGameRepository gameRep;
        private IOrganizationRepository organizationRep;
        private IStringLocalizer<GameController> loc;
        private IAccountRepository accountRep;
        private IHostingEnvironment hostingEnvironment;

        public GameController(IOrganizationRepository organizationRep, IGameRepository gameRep,
                              IStringLocalizer<GameController> loc, IAccountRepository accountRep,
                              IHostingEnvironment hostingEnvironment)
        {
            this.organizationRep = organizationRep;
            this.gameRep = gameRep;
            this.accountRep = accountRep;
            this.loc = loc;
            this.hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            List<Game> games = gameRep.GetAll().ToList();
            List<Organization> organizations = organizationRep.GetAll().ToList();

            GameViewModel gameVM = new GameViewModel() { Games = games, AllOrganizations = organizations };

            return View("Games", gameVM);
        }

        [HttpDelete]
        public IActionResult Delete(int gameId)
        {
            if (accountRep.GetAllByGameId(gameId).Count() != 0)
            {
                return RedirectToAction("Error", "Admin");//if this use
            }
            else
            {
                Game delGame = gameRep.GetById(gameId);
                if (delGame == null)
                {
                    return RedirectToAction("Error", "Admin");
                }
                else
                {
                    gameRep.Delete(gameId);
                    return RedirectToAction("Index");
                }
            }
        }

        [HttpPost]
        public IActionResult Update(int gameId, string bannerPath, string name, string dateString,
                             int organizationId, string description)
        {
            DateTime date;
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description)
                || !DateTime.TryParse(dateString, out date))
            {
                return RedirectToAction("Error", "Admin");
            }
            else
            {
                Game newGame = gameRep.GetById(gameId);
                if (bannerPath != null)
                {
                    newGame.Banner = bannerPath;
                }
                    newGame.Name = name;
                newGame.Date = date;
                newGame.Organization = organizationRep.GetById(organizationId);
                newGame.Description = description;
                gameRep.Update(newGame);
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Create(string bannerPath, string name, string dateString,
                             int organizationId, string description)
        {
            DateTime date;
            if (string.IsNullOrEmpty(bannerPath) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description)
                || !DateTime.TryParse(dateString, out date))
            {
                return RedirectToAction("Error", "Admin"); 
            }
            else
            {
                Game newGame = new Game()
                {
                    Banner = bannerPath,
                    Name = name,
                    Date = date,
                    Organization = organizationRep.GetById(organizationId),
                    Description = description
                };
                gameRep.Create(newGame);
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public string ImageUpload()
        {
            IFormFile uploadFile = Request.Form.Files.FirstOrDefault();
            string dicName = RandomUtil.RandomInt(15).ToString();
            if (uploadFile != null)
            {
                Directory.CreateDirectory(Path.GetDirectoryName(hostingEnvironment.WebRootPath + "/images/games/"
                                                                + dicName + "/"));
                using (var fileStream = new FileStream(hostingEnvironment.WebRootPath + "/images/games/"
                                                        + dicName + "/" + uploadFile.FileName, FileMode.Create))
                {
                    uploadFile.CopyToAsync(fileStream);
                }
            return ("/images/Games/" + dicName + '/' + uploadFile.FileName);
            }
            else
            {
                return null;
            }

        }
    }
}
