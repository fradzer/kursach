using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Business.Models;
using Microsoft.Extensions.Localization;

namespace AccountResales.Presentation.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class FounderController : Controller
    {
        private IFounderRepository founderRep;
        private readonly IStringLocalizer<FounderController> localizer;
        private IOrganizationRepository organizationRep;

        public FounderController(IFounderRepository founderRep, IOrganizationRepository organizationRep,
            IStringLocalizer<FounderController> loc)
        {
            this.founderRep = founderRep;
            this.organizationRep = organizationRep;
            localizer = loc;
        }


        public IActionResult Index()
        {
            List<Founder> founders = founderRep.GetAll().ToList();
            return View("Founders", founders);
        }

        [HttpPost]
        public IActionResult Create(string founderName)
        {
            if(string.IsNullOrEmpty(founderName) || founderName.Length < 3)
            {
                return RedirectToAction("Error", "Admin");
            }
            if (founderRep.GetByName(founderName) != null)
            {
                return RedirectToAction("Error", "Admin"); //if this founder contains
            }
            else
            {
                founderRep.Create(new Founder() {Name = founderName });
                return RedirectToAction("Index");
            }
        }

        [HttpDelete]
        public IActionResult Delete(int founderId)
        {
            if(organizationRep.GetAllByFounderId(founderId).Count() > 0)
            {
                return RedirectToAction("Error", "Admin"); // if this use
            }
            founderRep.Delete(founderId);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Update(int founderId, string founderName)
        {
            if (!string.IsNullOrEmpty(founderName) && founderName.Length < 3)
            {
                return RedirectToAction("Error", "Admin");
            }
            Founder founder = founderRep.GetById(founderId);
            founder.Name = founderName;
            founderRep.Update(founder);
            return RedirectToAction("Index");
        }
    }
}