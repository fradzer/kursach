﻿$(document).on("click", "#create-new-account", CreateAccount);
$(document).on("click", "#approve-account", ApproveAccount);
$(document).on("click", "#reject-account", RejectAccount);

var tickId;
function setAccountId(accountId) {
    tickId = accountId;
}


function CreateAccount() {
    var selectedGameId = $("#game-selected").val();
    var price = $("#price").val();
    if($.isNumeric(price) && price > 0)
    {
        var url = "/en/Account/Create";
        $.ajax({
            url: url,
            type:"POST",
            data: {
                gameId: selectedGameId,
                price: price
            },
            success: function () {
                location.reload(true);
            }
        });
    }
    else {
        $("#price").addClass("red-input");
    }
}

function DeleteAccount(accountId) {
    if (accountId > 0) {
        var url = "/en/Account/Delete";
        $.ajax({
            url: url,
            type: "DELETE",
            data: {
                accountId: accountId
            },
            success: function () {
                location.reload(true);

            }
        });
    }
}

function ApproveAccount(accountId) {
    var track = $("#track-account").val();
    if (tickId > 0 && track.length > 3) {
        var url = "/en/Account/Approve";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                accountId: tickId,
                track: track
            },
            success: function () {
                location.reload(true);

            }
        });
    }
    else {
        $("#track-account").addClass("red-input");
    }
}

function RejectAccount(accountId) {
    var comment = $("#comment-account").val();
    if (tickId > 0 && comment.length > 3) {
        var url = "/en/Account/Reject";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                accountId: tickId,
                comment: comment
            },
            success: function () {
                location.reload(true);

            }
        });
    }
    else {
        $("#track-account").addClass("red-input");
    }
}