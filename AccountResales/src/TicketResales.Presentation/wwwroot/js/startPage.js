$(document).on("click", ".filter-button", SendFilter);
$(document).on("click", ".pagination-button", SendPagination);
$(document).on("click", "#cancel-filter", CancelFilter);

var allGameNames;
var countGames;
var allOrganizations;
var allFounders;
const countGameView = 6;
var partialGameName;
var checkboxFounders = new Array();
var checkboxOrganizations = new Array();
var startDate = new Date();
var endDate = new Date();

var sortedSelect;

function RenderGames(games) {
    if (games.length < 1) {
        $(".game-cards").append(
                '<h2 style="margin-top:50px;">Sorry, but games not found :(</h2>'
                );
    }
    else {
        var length = games.length;
        if (countGameView < length) length = countGameView;
        for (var i = 0; i < length; i++) {
            var averagePrice = 0, k = 0;
            for (var j = 0; j < games[i].Accounts.length; j++) {
                if (games[i].Accounts[j].Status != 2) {
                    averagePrice += games[i].Accounts[j].Price;
                    k++;
                }
            }
            if (k > 0) {
                averagePrice = (averagePrice / k).toFixed(2);
            }
            else {
                averagePrice = '-';
            }

            $(".game-cards").append(
                '<a href="/' + getLang() + '/Home/Details?idGame=' + games[i].Game.Id + '" class="game-cart" style="background-image:url(' + games[i].Game.Banner + ');" id="game__' + games[i].Game.Id + '">' +
                    '<div class="info-cart">' +
                        '<div class="hide-element game-accounts-count">Accounts: ' + games[i].Accounts.length + '</div>' +
                        '<div class="total-info">' +
                            '<div class="game-name">' + games[i].Game.Name + '</div>' +
                            '<div class="game-date">Date: ' + (new Date(games[i].Game.Date)).toLocaleDateString("en-US") + '</div>' +
                            '<div class="game-average-price">Average price: ' + averagePrice + '</div>' +
                        '</div>' +
                    '</div>' +
                '</a>'
                );
        }

    }
}

function ClearGames() {
    $(".game-cards").empty();
}

function GetAllGamesNames() {
    var url = "/api/game/GetGamesNames";
    $.ajax({
        url: url,
        type: "GET",
        headers: { Accept: "application/json" },
        success: function (result) {
            allGameNames = result;
            countGames = allGameNames.length;
            UpdatePagination();
            setAutocompileGameName();

        }
    });
}

function GetAllFounders() {
    var url = "/api/founder/GetFounders";
    $.ajax({
        url: url,
        type: "GET",
        headers: { Accept: "application/json" },
        success: function (result) {
            allFounders = result;
            AddFoundersInCheckbox();
        }
    });
}

function GetAllOrganizations() {
    var url = "/api/organization/GetOrganizations";
    $.ajax({
        url: url,
        type: "GET",
        headers: { Accept: "application/json" },
        success: function (result) {
            allOrganizations = result;
            AddOrganizationsInCheckbox();
        }
    });
}

function SendFilter() {
    setFilter();
    setSort();

    var url = "/api/game/GetSuitableGames";

    $.ajax({
        url: url,
        type: "GET",
        headers: { Accept: "application/json" },
        data: {
            partialName: partialGameName,
            startDate: startDate,
            endDate: endDate,
            founderIds: checkboxFounders,
            organizationIds: checkboxOrganizations,
            sortBy: sortedSelect
        },
        traditional: true,
        success: function (result) {
            countGames = result.length;
            ClearGames();
            RenderGames(result);
            UpdatePagination();
        }
    });
}

function CancelFilter() {
    var url = "/api/game/GetAllGames";

    $.ajax({
        url: url,
        type: "GET",
        headers: { Accept: "application/json" },
        traditional: true,
        success: function (result) {
            countGames = result.length;
            ClearGames();
            RenderGames(result);
            UpdatePagination();
        }
    });
}

function SendPagination() {
    setFilter();
    setSort();

    var url = "/api/game/GetSuitableGames";

    $.ajax({
        url: url,
        type: "GET",
        headers: { Accept: "application/json" },
        data: {
            partialName: partialGameName,
            startDate: startDate,
            endDate: endDate,
            founderIds: checkboxFounders,
            organizationIds: checkboxOrganizations,
            sortBy: sortedSelect,
            pageNumber: $(this).attr("id")
        },
        traditional: true,
        success: function (result) {
            ClearGames(result);
            RenderGames(result);
        }
    });
}

function AddFoundersInCheckbox() {
    var founderCheckbox = document.getElementById("founders-checkbox-id");
    for (var i = 0; i < allFounders.length; i++) {
        var li = document.createElement('li');
        li.innerHTML = '<input type="checkbox" id="' + allFounders[i].Id + '" name="' + allFounders[i].Name + '" value="' + allFounders[i].Id + '"><label for="' + allFounders[i].Id + '">' + allFounders[i].Name + '</label>';
        founderCheckbox.appendChild(li);
    }
}

function AddOrganizationsInCheckbox() {
    var founderCheckbox = document.getElementById("organizations-checkbox-id");
    for (var i = 0; i < allOrganizations.length; i++) {
        var li = document.createElement('li');
        li.innerHTML = '<input type="checkbox" id="' + allOrganizations[i].Id + '" name="' + allOrganizations[i].Name + '" value="' + allOrganizations[i].Id + '"><label for="' + allOrganizations[i].Id + '">' + allOrganizations[i].Name + '</label>';
        founderCheckbox.appendChild(li);
    }
}

function UpdatePagination() {
    var paginationUl = document.getElementById("pagination");

    while (paginationUl.childNodes[0]) { paginationUl.removeChild(paginationUl.childNodes[0]); }

    for (var i = 0; i < Math.ceil(countGames / countGameView) ; i++) {
        var li = document.createElement("li");
        var number = (i + 1);
        li.innerHTML = "<a href='#' class='pagination-button' id='" + number + "'>" + number + "</a>";
        paginationUl.appendChild(li);
    }

}

function setAutocompileGameName() {
    $('#game-name-input').typeahead({
        hint: true,
        highlight: true,
        minLength: 0
    },
            {
                name: 'gameName',
                limit: 10,
                source: substringMatcher(allGameNames),
                templates: {
                    empty: [
                      '<div class="empty-message red-color">',
                        'Unable to find any game!',
                      '</div>'
                    ].join('\n')
                }
            });
}

function setFilter() {
    checkboxFounders = [];
    checkboxOrganizations = [];

    partialGameName = $("#game-name-input").val();

    $("#founders-checkbox-id input:checkbox:checked").each(function () {
        checkboxFounders.push(Number($(this).val()));
    });

    $("#organizations-checkbox-id input:checkbox:checked").each(function () {
        checkboxOrganizations.push(Number($(this).val()));
    });

    if ($(".start-date").val()) {
        startDate = (new Date($(".start-date").val())).toISOString();
    }
    else {
        startDate = undefined;
    }
    if ($(".end-date").val()) {
        endDate = (new Date($(".end-date").val())).toISOString();
    }
    else {
        endDate = undefined;
    }
}

function setSort() {
    sortedSelect = $(".sort .dropdown-menu input:radio:checked").val();
}

function addNotFoundMessage() {

}

function getLang() {
    var lang;
    var ru = "/ru";
    var be = "/be";
    var en = "/en";
    var a = location.href;
    if (a.indexOf(ru) == -1 && a.indexOf(en) == -1 && a.indexOf(be) == -1) {
        lang = "en";
    }
    else {
        if (a.indexOf(ru) != -1) lang = "ru";
        if (a.indexOf(en) != -1) lang = "en";
        if (a.indexOf(be) != -1) lang = "be";
    }
    return lang;
}

$(document).ready(function () {
    GetAllGamesNames();
    GetAllFounders();
    GetAllOrganizations();
});

var substringMatcher = function (strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};

var options = [];

$('.dropdown-menu a').on('click', function (game) {

    var $target = $(game.currentTarget),
        val = $target.attr('data-value'),
        $inp = $target.find('input'),
        idx;

    if ((idx = options.indexOf(val)) > -1) {
        options.splice(idx, 1);
        setTimeout(function () { $inp.prop('checked', false) }, 0);
    } else {
        options.push(val);
        setTimeout(function () { $inp.prop('checked', true) }, 0);
    }

    $(game.target).blur();

    console.log(options);
    return false;
});