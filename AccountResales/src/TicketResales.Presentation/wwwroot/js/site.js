﻿// Write your Javascript code.
$('#lang').change(ChangeLang);

function ChangeLang() {
    var lang = $("#lang").val();
    var returnUrl = location.href;
    var url = "/en/Home/ChangeLang";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            lang: lang,
            returnUrl: returnUrl
        },
        success: function () {
            var ru = "/ru";
            var en = "/en";
            var a = returnUrl;
            if (a.indexOf(ru) == -1 && a.indexOf(en) == -1) {
                location.href = (a + lang);
            }
            else {
                a = a.replace(ru, "/" + lang);
                a = a.replace(en, "/" + lang);
                location.href = a;
            }
        }
    });
}