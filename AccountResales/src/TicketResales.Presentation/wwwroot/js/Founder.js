$(document).on("click", "#create-new-founder", CreateFounder);
$(document).on("click", "#update-founder", Update);

var citId;
function setFounderId(id) {
    citId = id;
    var td = $("#founder-name__" + citId).html();
    $("#founder-name-update").val(td);
}
function CreateFounder() {
    var name = $("#new-founder-name");
    name.removeClass("red-input");
    nameval = name.val().replace(/^\s+|\s+$/g, '');
    if (!nameval || nameval.length < 3) {
        name.addClass("red-input");
    }
    else {
        var url = "/en/Admin/Founder/Create";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                founderName: nameval
            },
            success: function (data) {
                
                    location.reload(true)
            }
        });
    }
}

function DeleteFounder(id)
{
    var url = "/en/Admin/Founder/Delete";
    $.ajax({
        url: url,
        type: "DELETE",
        data: {
            founderId: id
        },
        success: function (data) {
            location.reload(true)

        }
    });
}

function Update() {
    var name = $("#founder-name-update");
    name.removeClass("red-input");
    nameval = name.val().replace(/^\s+|\s+$/g, '');
    if (!nameval || nameval.lenth < 3) {
        name.addClass("red-input");
    }
    else {
        var url = "/en/Admin/Founder/Update";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                founderId:citId,
                founderName: nameval
            },
            success: function (data) {
                location.reload(true)

            }
        });
    }
}