$(document).on("click", "#create-new-game", CreateGame);
$(document).on("click", "#update-game", UpdateGame);
var evId;

function CreateGame() {
    var selectedOrganization = $("#add-organization").val();

    var name = $("#add-name");
    name.removeClass("red-input");
    nameval = name.val().replace(/^\s+|\s+$/g, '');

    var date = new Date($("#add-date").val());
    var dateNowSec = new Date();
    var dateNow = new Date(dateNowSec.getFullYear(), dateNowSec.getMonth(), dateNowSec.getDate());
    $("#add-date").removeClass("red-input");

    var description = $("#add-description").val();
    $("#add-description").removeClass("red-input");
    descriptionval = description.replace(/^\s+|\s+$/g, '');

    if (nameval && nameval.length > 2) {
        if (date != 'Invalid Date' && date > dateNow) {
            if (description && description.length > 2) {
                var uploadfile = document.getElementById('input-2-add').files;
                var data = new FormData();
                for (var x = 0; x < uploadfile.length; x++) {
                    data.append("file" + x, uploadfile[x]);
                }
                var url = "/en/Admin/Game/ImageUpload";
                $.ajax({
                    type: "POST",
                    url: url,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        if (result.indexOf('/') + 1) {
                            var url = "/en/Admin/Game/Create";
                            $.ajax({
                                url: url,
                                type: "POST",
                                data: {
                                    bannerPath: result,
                                    name: nameval,
                                    dateString: date.toDateString(),
                                    organizationId: selectedOrganization,
                                    description: descriptionval

                                },
                                success: function (data) {
                                    location.reload(true)

                                }
                            });
                        }
                        else {
                            $(".add-warnings-game").text(result);
                            $(".add-warnings-game").addClass("text-red");
                        }
                    }
                });

            }

            else {
                $("#add-description").addClass("red-input");
            }
        }
        else {
            $("#add-date").addClass("red-input");
        }
    }
    else {
        $("#add-name").addClass("red-input");
    }
}

function UpdateGame() {
    var selectedOrganization = $("#update-organization").val();

    var name = $("#update-name");
    name.removeClass("red-input");
    nameval = name.val().replace(/^\s+|\s+$/g, '');

    var date = new Date($("#update-date").val());
    var dateNowSec = new Date();
    var dateNow = new Date(dateNowSec.getFullYear(), dateNowSec.getMonth(), dateNowSec.getDate());
    $("#update-date").removeClass("red-input");

    var description = $("#update-description").val();
    $("#update-description").removeClass("red-input");
    descriptionval = description.replace(/^\s+|\s+$/g, '');

    if (nameval && nameval.length > 2) {
        if (date != 'Invalid Date' && date > dateNow) {
            if (description && description.length > 2) {
                var uploadfile = document.getElementById('input-2-update').files;
                var data = new FormData();
                for (var x = 0; x < uploadfile.length; x++) {
                    data.append("file" + x, uploadfile[x]);
                }
                var url = "/en/Admin/Game/ImageUpload";
                $.ajax({
                    type: "POST",
                    url: url,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        var banner = null;
                        if (result && (result.indexOf('/') + 1))
                        {
                            banner = result;
                        }
                        var url = "/en/Admin/Game/Update";
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: {
                                gameId: evId,
                                bannerPath: banner,
                                name: nameval,
                                dateString: date.toDateString(),
                                organizationId: selectedOrganization,
                                description: descriptionval

                            },
                            success: function (data) {
                                location.reload(true)

                            }
                        });
                    }
                });

            }

            else {
                $("#update-description").addClass("red-input");
            }
        }
        else {
            $("#update-date").addClass("red-input");
        }
    }
    else {
        $("#update-name").addClass("red-input");
    }
}

function setGame(gameId, organizationId) {
    $(".show-image img").attr('src', $("tr #game-banner__" + gameId + " img.banner-small").attr("src"));

    $("#update-name").val($("#game-name__" + gameId).text());
    $("select option[value=" + organizationId + "]").attr('selected', 'true');

    $("#update-date").val($("#game-date__" + gameId).text());
    $("#update-description").val($("#game-description__" + gameId).text());
    evId = gameId;
}

function DeleteGame(gameId) {
    if (gameId > 0) {
        var url = "/en/Admin/Game/Delete";
        $.ajax({
            url: url,
            type: "DELETE",
            data: {
                gameId: gameId
            },
            success: function (data) {
                location.reload(true)

            }
        });
    }
}
