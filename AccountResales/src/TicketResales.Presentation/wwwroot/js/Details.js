function BoughtAccount(accountId, messageForConfirm) {
    if (confirm(messageForConfirm)) {
        if (accountId > 0) {
            var url = "/en/Home/BoughtAccount";
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    accountId: accountId,
                    returnUrl: location.href.toString()
                },
                success: function () {
                    location.reload(true);
                }
            });
        }
    }
}