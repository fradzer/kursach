function RevokeUser(userId) {
    var url = "/en/Admin/User/Revoke";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            userId: userId
        },
        success: function () {
            location.reload(true);
        }
    });
}

function GrandUser(userId) {
    var url = "/en/Admin/User/Grand";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            userId: userId
        },
        success: function () {
            location.reload(true);
        }
    });
}