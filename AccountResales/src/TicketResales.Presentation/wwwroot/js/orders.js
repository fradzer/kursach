﻿function DeleteOrder(orderId) {
    if (orderId > 0) {
        var url = "/en/Order/Delete";
        $.ajax({
            url: url,
            type: "DELETE",
            data: {
                orderId: orderId
            },
            success: function (data) {
                if (data) {
                    $("tr#order-id__" + orderId).remove();
                }
            }
        });
    }
}

function ViewComment(textComment) {
    $("#text-comment").text(textComment);
}