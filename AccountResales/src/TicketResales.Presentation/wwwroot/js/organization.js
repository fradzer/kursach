$(document).on("click", "#create-new-organization", CreateOrganization);
$(document).on("click", "#update-new-organization", UpdateOrganization);

var venId;

function setDataOrganization(organizationId, founderId, organizationAddress, organizationName) {
    venId = organizationId;

    $("#update-address").val(organizationAddress);
    $("#update-name").val(organizationName);
    $("select option[value=" + founderId + "]").attr('selected', 'true');

}


function CreateOrganization() {
    var selectedOrganization = $("#add-founder").val();

    var name = $("#add-name");
    name.removeClass("red-input");
    nameval = name.val().replace(/^\s+|\s+$/g, '');

    var address = $("#add-address");
    address.removeClass("red-input");
    addressval = address.val().replace(/^\s+|\s+$/g, '');
    if (addressval && addressval.length > 2) {
        if (nameval && nameval.length > 2) {
            var url = "/en/Admin/Organization/Create";
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    founderId: selectedOrganization,
                    name: nameval,
                    address: addressval
                },
                success: function (data) {
                    location.reload(true)

                }
            });
        }
        else {
            $("#add-name").addClass("red-input");
        }
    }
    else {
        $("#add-address").addClass("red-input");
    }
}

function UpdateOrganization() {
    var selectedOrganization = $("#update-founder").val();

    var name = $("#update-name");
    name.removeClass("red-input");
    nameval = name.val().replace(/^\s+|\s+$/g, '');

    var address = $("#update-address");
    address.removeClass("red-input");
    addressval = address.val().replace(/^\s+|\s+$/g, '');
    if (addressval && addressval.length > 2) {
        if (nameval && nameval.length > 2) {
            var url = "/en/Admin/Organization/Update";
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    organizationId: venId,
                    founderId: selectedOrganization,
                    name: nameval,
                    address: addressval
                },
                success: function (data) {
                    location.reload(true)

                }
            });
        }
        else {
            $("#update-name").addClass("red-input");
        }
    }
    else {
        $("#update-address").addClass("red-input");
    }
}

function DeleteOrganization(organizationId) {
    if (organizationId > 0) {
        var url = "/en/Admin/Organization/Delete";
        $.ajax({
            url: url,
            type: "DELETE",
            data: {
                organizationId: organizationId
            },
            success: function (data) {
                location.reload(true)

            }
        });
    }
}
