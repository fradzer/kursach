﻿$(document).on("click", ".save-info", SaveInfo);
$(document).on("click", ".change-password-href", ChangePassword);


function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^\w+@\w+\.\w{2,4}$/i);
    return pattern.test(emailAddress);
}


function isValidInformation(email, firstName, lastName, address, phone) {
    var trueInfo = true;
    //if (!isValidEmailAddress(email.val())) {
    //    email.addClass("red-input");
    //    trueInfo = false;
    //}
    return trueInfo;
}

function SaveInfo() {
    var userId = this.id;
    //var email = $("#user-email");
    var firstName = $("#user-first-name");
    var lastName = $("#user-last-name");
    var address = $("#user-address");
    var phone = $("#user-phone");

    //email.removeClass("red-input");
    firstName.removeClass("red-input");
    lastName.removeClass("red-input");
    address.removeClass("red-input");
    phone.removeClass("red-input");

    if (isValidInformation(/*email,*/ firstName, lastName, address, phone)) {
        var url = "/en/UserAccount/SaveInfo";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                userId: userId,
                //email: email.val(),
                firstName: firstName.val(),
                lastName: lastName.val(),
                address: address.val(),
                phone: phone.val()
            },
            success: function (data) {
                $("#warnings").text(data);
                $("#warnings").removeAttr("style")
                setTimeout(function () { $('#warnings').fadeOut('fast') }, 5000);
            }
        });
    }
}

function ChangePassword()
{
    var userId = this.id;
    var oldPassword = $("#old-password");
    var oldPasswordVal = oldPassword.val().replace(/\s+/g, '');

    var newPassword = $("#new-password");
    var newPasswordVal = newPassword.val().replace(/\s+/g, '');

    var confirmNewPassword = $("#confirm-new-password");
    var confirmNewPasswordVal = confirmNewPassword.val().replace(/\s+/g, '');

    oldPassword.removeClass("red-input");
    newPassword.removeClass("red-input");
    confirmNewPassword.removeClass("red-input");
    var a = oldPassword.val();
    if (oldPasswordVal.length == 0 && oldPasswordVal == '') {
        oldPassword.addClass("red-input");
    }
    else {
        if (newPasswordVal.length == 0 && newPasswordVal == '') {
            newPassword.addClass("red-input");
        }
        else {
            if (newPasswordVal != confirmNewPasswordVal) {
                confirmNewPassword.addClass("red-input");
            }
            else {
                var url = "/en/UserAccount/ChangePassword";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        userId: userId,
                        oldPassword: oldPassword.val(),
                        newPassword: newPassword.val(),
                        confirmNewPassword: confirmNewPassword.val()
                    },
                    success: function (data) {
                        $("#warnings-password").text(data);
                        $("#warnings-password").removeAttr("style")
                        setTimeout(function () { $('#warnings-password').fadeOut('fast') }, 5000);
                    }
                });
            }
        }
    }

}