using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Business.Models;
using AccountResales.Data.Utils;
using System.Web.Http;

namespace AccountResales.Presentation.ApiControllers
{
    [Route("api/Founder")]
    public class FounderController : ApiController
    {
        private IFounderRepository founderRep;

        public FounderController(IFounderRepository founderRep)
        {
            this.founderRep = founderRep;
        }

        [HttpGet]
        [Route("GetFounders")]
        public IActionResult GetFounders([FromHeader]string Accept)
        {
            List<Founder> founders = founderRep.GetAll().ToList();
            var formatResult = ParseUtil.ParseToStringByType<List<Founder>>(founders, 
                                            (ParseType)Enum.Parse(typeof(ParseType), ParseUtil.GetFormatFromAcceptString(Accept.ToLower())));
            return new ObjectResult(formatResult);
        }
    }
}