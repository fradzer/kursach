using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http;
using AccountResales.Data.Repositories.Interfaces;
using AccountResales.Business.Models;
using AccountResales.Data.Utils;

namespace AccountResales.Presentation.ApiControllers
{
    [Route("api/Organization")]
    public class OrganizationController : ApiController
    {
        private IOrganizationRepository organizationRep;

        public OrganizationController(IOrganizationRepository organizationRep)
        {
            this.organizationRep = organizationRep;
        }

        [HttpGet]
        [Route("GetOrganizations")]
        public IActionResult GetOrganizations([FromHeader]string Accept)
        {
            List<Organization> organizations = organizationRep.GetAll().ToList();
            var formatResult = ParseUtil.ParseToStringByType<List<Organization>>(organizations, 
                                            (ParseType)Enum.Parse(typeof(ParseType), ParseUtil.GetFormatFromAcceptString(Accept.ToLower())));
            return new ObjectResult(formatResult);
        }
    }
}