using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AccountResales.Business.Models;
using AccountResales.Data.Repositories.Interfaces;
using Newtonsoft.Json;
using AccountResales.Data.Utils;
using AccountResales.Data.Filters;
using AccountResales.Data.Services;
using System.Net.Http;
using System.Net.Http.Headers;
using AccountResales.Presentation.ApiService;
using AccountResales.Data.Constants;
using AccountResales.Presentation.Models.GameViewModels;

namespace AccountResales.Presentation.ApiControllers
{
    [Route("api/Game")]
    public class GameController : ApiController
    {
        private IGameRepository gameRep;
        private GameFilterService gameFilterService;
        private GameSortService gameSortService;
        private IAccountRepository accountRep;

        public GameController(IGameRepository gameRep, IAccountRepository accountRep, GameFilterService gameFilterService,
                               GameSortService gameSortService)
        {
            this.gameRep = gameRep;
            this.gameFilterService = gameFilterService;
            this.gameSortService = gameSortService;
            this.accountRep = accountRep;
        }

        [HttpGet]
       // [ResponseCache type]
        [Route("GetSuitableGames")]
        public IActionResult GetSuitableGames([FromHeader]string Accept, [FromQuery]string partialName,
                                               [FromQuery]DateTime startDate, [FromQuery]DateTime endDate,
                                               [FromQuery]List<int> founderIds, [FromQuery]List<int> organizationIds,
                                               [FromQuery]string sortBy = "date", [FromQuery]int limit = GameConstants.CountGamesForView, 
                                               [FromQuery]int pageNumber = 0)
        {

            List<GameDetailsViewModel> gamesDetails = new List<GameDetailsViewModel>();
            GameFilter filter = new GameFilter()
            {
                PartialName = partialName,
                StartDate = startDate,
                EndDate = endDate,
                FounderIds = founderIds,
                OrganizationIds = organizationIds
            };
            List<Game> filterResult = gameFilterService.GetSuitableGames(gameRep.GetAll(), filter).ToList();

            filterResult = gameSortService.SortGamesByFlag(filterResult, sortBy).ToList();

            if (pageNumber > 0)
            {
                int offset = (pageNumber - 1) * limit;
                filterResult = filterResult.Skip(offset).Take(limit).ToList();
            }
            filterResult.ForEach(x => gamesDetails.Add(new GameDetailsViewModel() { Game = x, Accounts = accountRep.GetAllByGameId(x.Id).ToList() }));

            var formatResult = ParseUtil.ParseToStringByType<List<GameDetailsViewModel>>(gamesDetails, (ParseType)Enum.Parse(typeof(ParseType),
                                                                                ParseUtil.GetFormatFromAcceptString(Accept.ToLower())));

            return new ObjectResult(formatResult);
        }

        [HttpGet]
        [Route("GetAllGames")]
        public IActionResult GetAllGames([FromHeader]string Accept)
        {
            List<Game> games = gameRep.GetAll().ToList();
            games = gameSortService.SortGamesByDate(games).ToList();
            List<GameDetailsViewModel> gamesDetails = new List<GameDetailsViewModel>();


            games.ForEach(x => gamesDetails.Add(new GameDetailsViewModel() { Game = x, Accounts = accountRep.GetAllByGameId(x.Id).ToList() }));

            var formatResult = ParseUtil.ParseToStringByType<List<GameDetailsViewModel>>(gamesDetails,
                                           (ParseType)Enum.Parse(typeof(ParseType), ParseUtil.GetFormatFromAcceptString(Accept.ToLower())));
            return new ObjectResult(formatResult);
        }

        [HttpGet]
        [Route("GetGamesNames")]
        public IActionResult GetGamesNames([FromHeader]string Accept)
        {
            //List<ApiGameName> gamesNames = new List<ApiGameName>();

            //gameRep.GetAll().ToList().ForEach(x => gamesNames.Add(new ApiGameName() { Id = x.Id, Name = x.Name }));
            List<string> gamesNames = gameRep.GetAll().Select(x => x.Name).ToList();

            var formatResult = ParseUtil.ParseToStringByType<List<string>>(gamesNames,
                                            (ParseType)Enum.Parse(typeof(ParseType), ParseUtil.GetFormatFromAcceptString(Accept.ToLower())));
            return new ObjectResult(formatResult);
        }

        //[HttpGet]
        //[Route("GetPaginationGame")]
        //public async Task<IActionResult> GetPaginationGame([FromHeader]string Accept,
        //                                       [FromQuery]string partialName, [FromQuery]DateTime startDate,
        //                                       [FromQuery]DateTime endDate, [FromQuery]List<int> founderIds,
        //                                       [FromQuery]List<int> organizationIds, [FromQuery]string sortBy,
        //                                       [FromQuery]int limit = 4, [FromQuery]int pageNumber = 1)
        //{
        //    int offset = (pageNumber - 1) * limit;

        //    List<Game> filterGames = null;

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("http://" + HttpContext.Request.Host.Value);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        var uriBuilder = new UriBuilder();
        //        uriBuilder = UriService.AddParameter(uriBuilder, "partialName", partialName);
        //        uriBuilder = UriService.AddParameter(uriBuilder, "startDate", startDate.Date.ToString());
        //        uriBuilder = UriService.AddParameter(uriBuilder, "endDate", endDate.Date.ToString());
        //        uriBuilder = UriService.AddParameter(uriBuilder, "sortBy", sortBy);

        //        founderIds.ForEach(x => { uriBuilder = UriService.AddParameter(uriBuilder, "founderIds", x.ToString()); });
        //        organizationIds.ForEach(x => { uriBuilder = UriService.AddParameter(uriBuilder, "organizationIds", x.ToString()); });

        //        HttpResponseMessage response = await client.GetAsync("/api/v1/game/GetFilteredGames/" + uriBuilder.Query);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            filterGames = (await response.Content.ReadAsAsync<IEnumerable<Game>>()).ToList();
        //        }
        //    }
        //    return new ObjectResult(filterGames.Skip(offset).Take(limit).ToList());


        //}
    }
}