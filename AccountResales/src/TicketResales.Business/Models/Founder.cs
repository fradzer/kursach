﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountResales.Business.Models
{
    public class Founder
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
