﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AccountResales.Business.Models
{
    public enum StatusAccount
    {
        [Description("Selling")]
        Selling = 0,
        [Description("Waiting confirmation")]
        WaitingConfirmation = 1,
        [Description("Sold")]
        Sold = 2
    }

    public class Account
    {
        public int Id { get; set; }

        public Game Game { get; set; }

        public decimal Price { get; set; }

        public User Seller { get; set; }

        public StatusAccount Status { get; set; }
    }

    
}
