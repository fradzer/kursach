﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountResales.Business.Models
{
    public class Organization
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Founder Founder { get; set; }
    }
}