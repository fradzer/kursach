﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountResales.Business.Models
{
    //public enum Localization
    //{
    //    en = 0,
    //    ru = 1,
    //    by = 2
    //}
    
    public class User
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        public Role Role { get; set; }

        public string PasswordHash { get; set; }

        public string Localization { get; set; }
    }
}
