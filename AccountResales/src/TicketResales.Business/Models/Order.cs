﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AccountResales.Business.Models
{
    public enum StatusOrder{
        [Description("Waitiong confirmation")]
        WaitingConfirmation,
        [Description("Confirmed")]
        Confirmed,
        [Description("Rejected")]
        Rejected
    }

    public class Order
    {
        public int Id { get; set; }

        public Account Account { get; set; }

        public StatusOrder Status { get; set; }
        
        public User Buyer { get; set; }

        public string Track { get; set; }
        public string Comment { get; set; }
    }
}
