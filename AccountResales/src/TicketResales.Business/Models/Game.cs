﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountResales.Business.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }

        public Organization Organization { get; set; }

        public string Banner { get; set; }
        public string Description { get; set; }
    }
}
