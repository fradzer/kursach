﻿
using AccountResales.Business.Models;
using AccountResales.Data.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Data.Services
{
    public class GameFilterService
    {
        public GameFilterService()
        {

        }
        public IEnumerable<Game> GetSuitableGames(IEnumerable<Game> games, GameFilter filter)
        {
            var filterGame = games.AsQueryable();

            filterGame = GetGamesByPartialName(filterGame, filter.PartialName);
            filterGame = GetGamesByFounderIds(filterGame, filter.FounderIds);
            filterGame = GetGamesByOrganizationIds(filterGame, filter.OrganizationIds);
            filterGame = GetGamesByDateRange(filterGame, filter.StartDate, filter.EndDate);
            return filterGame.ToList();
        }

        public IQueryable<Game> GetGamesByDateRange(IQueryable<Game> games, DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null || startDate < DateTime.Now)
            {
                startDate = DateTime.Now;
            }
            if (endDate == null || endDate == default(DateTime))
            {
                endDate = DateTime.MaxValue;
            }
            if(games == null || startDate > endDate)
            {
                return games;
            }
            else
            {
                return FindByPredicate(games, x => (x.Date.Date >= startDate.Value.Date && x.Date.Date <= endDate.Value.Date));
            }
        }

        public static IQueryable<Game> GetGamesByPartialName(IQueryable<Game> games, string partialName)
        {
            if (games == null || string.IsNullOrWhiteSpace(partialName))
            {
                return games;
            }
            else
            {
                return FindByPredicate(games, x => x.Name.ToUpper().Contains(partialName.ToUpper()));
            }
        }

        public static IQueryable<Game> GetGamesByFounderIds(IQueryable<Game> games, List<int> founderIds)
        {
            if (games == null || founderIds == null || !founderIds.Any())
            {
                return games;
            }
            else
            {
                return FindByPredicate(games, x => founderIds.Contains(x.Organization.Founder.Id));
            }
        }

        public static IQueryable<Game> GetGamesByOrganizationIds(IQueryable<Game> games, List<int> organizationIds)
        {
            if (games == null || organizationIds == null || !organizationIds.Any())
            {
                return games;
            }
            else
            {
                return FindByPredicate(games, x => organizationIds.Contains(x.Organization.Id));
            }
        }

        private static IQueryable<Game> FindByPredicate(IQueryable<Game> games, Func<Game,bool> predicate)
        {
            return games.Where(predicate).AsQueryable();
        } 
    }
}
