﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Data.Services
{
    public class GameSortService
    {
        public IEnumerable<Game> SortGamesByDate(IEnumerable<Game> games)
        {
            return games.OrderBy(x => x.Date);
        }

        public IEnumerable<Game> SortGamesByName(IEnumerable<Game> games)
        {
            return games.OrderBy(x => x.Name);
        }

        public IEnumerable<Game> SortGamesByFlag(IEnumerable<Game> filterResult, string sortBy)
        {
            if(string.IsNullOrWhiteSpace(sortBy) || filterResult == null || !filterResult.Any())
            {
                return filterResult;
            }
            else
            {
                switch (sortBy.ToLower())
                {
                    case "name":
                        {
                            return SortGamesByName(filterResult).ToList();
                        }
                    case "date":
                        {
                            return SortGamesByDate(filterResult).ToList();
                        }
                    default:
                        {
                            throw new ArgumentException("sortBy");
                        }
                }
            }
        }
    }
}
