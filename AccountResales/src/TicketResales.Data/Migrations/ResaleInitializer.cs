﻿using AccountResales.Business.Models;
using AccountResales.Data.Repositories.MSSQLRepositories;
using AccountResales.Data.Utils;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Data.Migrations
{
    public static class ResaleInitializer
    {
        public static void Ititialize(this IApplicationBuilder app)
        {
            var context = (ResaleDbContext)app.ApplicationServices.GetService(typeof(ResaleDbContext));

            clearDatabase(context);
            seedFounderTable(context);
            seedOrganizationTable(context);
            seedGameTable(context);
            seedRoleTable(context);
            seedUserTable(context);
            seedAccountTable(context);
            seedOrderTable(context);
        }

        private static void clearDatabase(ResaleDbContext context)
        {
            context.Orders.RemoveRange(context.Orders);
            context.Accounts.RemoveRange(context.Accounts);
            context.Users.RemoveRange(context.Users);
            context.Roles.RemoveRange(context.Roles);
            context.Games.RemoveRange(context.Games);
            context.Organizations.RemoveRange(context.Organizations);
            context.Founders.RemoveRange(context.Founders);
            context.SaveChanges();
        }

        private static void seedFounderTable(ResaleDbContext context)
        {
            context.Founders.AddRange(
                new Founder() { Name = "Scott Foe" },
                new Founder() { Name = "Tommy Palm" },
                new Founder() { Name = "Rami Ismail" },
                new Founder() { Name = "Ilari Kuittinen" },
                new Founder() { Name = "Wilhelm Taht" },
                new Founder() { Name = "Tim Symons" }
               );
            context.SaveChanges();
        }

        private static void seedOrganizationTable(ResaleDbContext context)
        {
            context.Organizations.AddRange(
                new Organization() { Name = "Blizzard Entertainment", Address = "street 2, 25", Founder = context.Founders.ToList()[0] },
                new Organization() { Name = "Game Founders", Address = "street 6, 33", Founder = context.Founders.ToList()[0] },
                new Organization() { Name = "Monolog", Address = "street 30, 17", Founder = context.Founders.ToList()[1] },
                new Organization() { Name = "Ubisoft Montreal", Address = "street 3, 3", Founder = context.Founders.ToList()[1] },
                new Organization() { Name = "Stygma", Address = "street 12, 25", Founder = context.Founders.ToList()[2] },
                new Organization() { Name = "2K Games", Address = "street 3, 21", Founder = context.Founders.ToList()[2] },
                new Organization() { Name = "Motic", Address = "street 222, 35", Founder = context.Founders.ToList()[3] },
                new Organization() { Name = "Sumo Digital", Address = "street 12, 15", Founder = context.Founders.ToList()[3] },
                new Organization() { Name = "Flash", Address = "street 36, 11", Founder = context.Founders.ToList()[4] },
                new Organization() { Name = "Electronic Arts", Address = "street 2, 25", Founder = context.Founders.ToList()[4] },
                new Organization() { Name = "Strategy", Address = "street 55, 17", Founder = context.Founders.ToList()[5] },
                new Organization() { Name = "Crytek", Address = "street 21, 25", Founder = context.Founders.ToList()[5] }
                );
            context.SaveChanges();
        }

        private static void seedGameTable(ResaleDbContext context)
        {
            string commonDescr = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit.In tellus diam,
                                                ultrices nec rutrum sit amet, sollicitudin vitae erat.Etiam aliquam
                                                lacus ac justo efficitur laoreet sit amet ac quam.Mauris lorem libero,
                                                tristique quis elit luctus, aliquam feugiat mi.Vestibulum lectus leo,
                                                tincidunt ac urna vitae, dapibus sollicitudin libero.Integer eu ligula
                                                vitae risus consectetur lobortis.Nunc lacus arcu, scelerisque non rhoncus in,
                                                vestibulum et purus.Nullam orci justo, sodales ac pellentesque nec, mattis 
                                                tincidunt odio.In blandit sapien risus, a tristique metus fringilla id.
                                                Suspendisse sit amet pharetra dolor.";

            context.Games.AddRange(
                new Game() { Organization = context.Organizations.ToList()[1], Name = "Speech", Date = DateTime.Now.AddDays(5), Description = commonDescr, Banner = "/images/Games/event1.jpg" },
                new Game() { Organization = context.Organizations.ToList()[2], Name = "Beach Party", Date = DateTime.Now.AddDays(7), Description = commonDescr, Banner = "/images/Games/event2.jpg" },
                new Game() { Organization = context.Organizations.ToList()[3], Name = "USSR", Date = DateTime.Now.AddDays(11), Description = commonDescr, Banner = "/images/Games/event3.jpg" },
                new Game() { Organization = context.Organizations.ToList()[11], Name = "By DotA", Date = DateTime.Now.AddDays(3), Description = commonDescr, Banner = "/images/Games/event4.jpg" },
                new Game() { Organization = context.Organizations.ToList()[5], Name = "Horror crazy notification", Date = DateTime.Now.AddDays(2), Description = commonDescr, Banner = "/images/Games/event5.jpg" },

                new Game() { Organization = context.Organizations.ToList()[4], Name = "David Blaine show!", Date = DateTime.Now.AddDays(4), Description = commonDescr, Banner = "/images/Games/event6.jpg" },
                new Game() { Organization = context.Organizations.ToList()[6], Name = "Space Museum", Date = DateTime.Now.AddDays(10), Description = commonDescr, Banner = "/images/Games/event7.jpg" },
                new Game() { Organization = context.Organizations.ToList()[7], Name = "Link", Date = DateTime.Now.AddDays(30), Description = commonDescr, Banner = "/images/Games/event8.jpg" },
                new Game() { Organization = context.Organizations.ToList()[4], Name = "Sympho", Date = DateTime.Now.AddDays(8), Description = commonDescr, Banner = " /images/Games/event9.jpg" },
                new Game() { Organization = context.Organizations.ToList()[6], Name = "Jimmy", Date = DateTime.Now.AddDays(6), Description = commonDescr, Banner = "/images/Games/event10.jpg" },
                new Game() { Organization = context.Organizations.ToList()[1], Name = "Make Yourself", Date = DateTime.Now.AddDays(7), Description = commonDescr, Banner = "/images/Games/event11.jpg" },
                new Game() { Organization = context.Organizations.ToList()[2], Name = "Evol", Date = DateTime.Now.AddDays(22), Description = commonDescr, Banner = "/images/Games/event12.jpg" },
                new Game() { Organization = context.Organizations.ToList()[1], Name = "Developer17", Date = DateTime.Now.AddDays(3), Description = commonDescr, Banner = "/images/Games/event13.jpg" },
                new Game() { Organization = context.Organizations.ToList()[11], Name = "Young Parent", Date = DateTime.Now.AddDays(4), Description = commonDescr, Banner = "/images/Games/event14.jpg" }



                );
            context.SaveChanges();
        }

        private static void seedRoleTable(ResaleDbContext context)
        {
            context.Roles.AddRange(
                new Role() { Name = "User" },
                new Role() { Name = "Admin" }
                );
            context.SaveChanges();
        }

        private static void seedUserTable(ResaleDbContext context)
        {
            context.Users.AddRange(
                new User()
                {
                    Email = "email1@email.ru",
                    FirstName = "Alex",
                    LastName = "LastAlex",
                    Localization = "ru",
                    PasswordHash = PasswordUtil.GetSHA1HashString("password1"),
                    Role = context.Roles.ToList()[1],
                    Address = "Gomel Street 5",
                    Phone = "+35555555"
                },
                new User()
                {
                    Email = "email2@email.ru",
                    FirstName = "Mark",
                    LastName = "User",
                    Localization = "be",
                    PasswordHash = PasswordUtil.GetSHA1HashString("User"),
                    Role = context.Roles.ToList()[1],
                    Address = "Gomel Street 5",
                    Phone = "+35555555"
                },
                new User()
                {
                    Email = "admin@admin.ru",
                    FirstName = "Admin",
                    LastName = "Admin",
                    Localization = "en",
                    PasswordHash = PasswordUtil.GetSHA1HashString("admin"),
                    Role = context.Roles.ToList()[0],
                    Address = "Gomel Street 5",
                    Phone = "+35555555"
                }
                );
            context.SaveChanges();
        }

        private static void seedAccountTable(ResaleDbContext context)
        {
            Random rand = new Random();

            context.Accounts.AddRange(
                new Account()
                {
                    Price = 150000,
                    Seller = context.Users.ToList()[1],
                    Status = StatusAccount.Sold,
                    Game = context.Games.ToList()[0]
                },
                new Account()
                {
                    Price = 200000,
                    Seller = context.Users.ToList()[1],
                    Status = StatusAccount.Sold,
                    Game = context.Games.ToList()[1]
                },
                new Account()
                {
                    Price = 300000,
                    Seller = context.Users.ToList()[0],
                    Status = StatusAccount.Sold,
                    Game = context.Games.ToList()[2]
                },
                new Account()
                {
                    Price = 420000,
                    Seller = context.Users.ToList()[0],
                    Status = StatusAccount.Sold,
                    Game = context.Games.ToList()[2]
                }
                );

            for (int i = 0; i < 100; i++)
            {
                context.Accounts.Add(
                    new Account()
                    {
                        Price = rand.Next(30000, 500000),
                        Seller = context.Users.ToList()[rand.Next(0, (context.Users.Count() - 1))],
                        Status = StatusAccount.Selling,
                        Game = context.Games.ToList()[rand.Next(0, (context.Games.Count() - 1))]
                    }
                    );
            }

            context.SaveChanges();
        }

        private static void seedOrderTable(ResaleDbContext context)
        {
            context.Orders.AddRange(
                new Order() { Buyer = context.Users.ToList()[0], Status = StatusOrder.Confirmed, Account = context.Accounts.ToList()[1] },
                new Order()
                {
                    Buyer = context.Users.ToList()[0],
                    Status = StatusOrder.Rejected,
                    Account = context.Accounts.ToList()[2],
                    Comment = "Sorry, but account already do not sold"
                },
                new Order()
                {
                    Buyer = context.Users.ToList()[1],
                    Status = StatusOrder.Rejected,
                    Account = context.Accounts.ToList()[3],
                    Comment = "Sorry, but sdsdddadadsa sa as as as"
                },
                new Order()
                {
                    Buyer = context.Users.ToList()[1],
                    Status = StatusOrder.Rejected,
                    Account = context.Accounts.ToList()[4],
                    Comment = "Sorry"
                }
                );
            context.SaveChanges();
        }
    }
}
