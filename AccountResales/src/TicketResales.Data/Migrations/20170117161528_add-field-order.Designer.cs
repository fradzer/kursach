﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using AccountResales.Data.Repositories.MSSQLRepositories;
using AccountResales.Business.Models;

namespace AccountResales.Data.Migrations
{
    [DbContext(typeof(ResaleDbContext))]
    [Migration("20170117161528_add-field-order")]
    partial class addfieldorder
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AccountResales.Business.Models.Founder", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Founders");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Game", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Banner");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int?>("OrganizationId");

                    b.HasKey("Id");

                    b.HasIndex("OrganizationId");

                    b.ToTable("Games");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BuyerId");

                    b.Property<string>("Comment");

                    b.Property<int>("Status");

                    b.Property<int?>("AccountId");

                    b.Property<string>("Track");

                    b.HasKey("Id");

                    b.HasIndex("BuyerId");

                    b.HasIndex("AccountId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Account", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("GameId");

                    b.Property<decimal>("Price");

                    b.Property<int?>("SellerId");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.HasIndex("SellerId");

                    b.ToTable("Accounts");
                });

            modelBuilder.Entity("AccountResales.Business.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("Phone");

                    b.Property<int?>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Organization", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int?>("FounderId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("FounderId");

                    b.ToTable("Organizations");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Game", b =>
                {
                    b.HasOne("AccountResales.Business.Models.Organization", "Organization")
                        .WithMany()
                        .HasForeignKey("OrganizationId");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Order", b =>
                {
                    b.HasOne("AccountResales.Business.Models.User", "Buyer")
                        .WithMany()
                        .HasForeignKey("BuyerId");

                    b.HasOne("AccountResales.Business.Models.Account", "Account")
                        .WithMany()
                        .HasForeignKey("AccountId");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Account", b =>
                {
                    b.HasOne("AccountResales.Business.Models.Game", "Game")
                        .WithMany()
                        .HasForeignKey("GameId");

                    b.HasOne("AccountResales.Business.Models.User", "Seller")
                        .WithMany()
                        .HasForeignKey("SellerId");
                });

            modelBuilder.Entity("AccountResales.Business.Models.User", b =>
                {
                    b.HasOne("AccountResales.Business.Models.Role", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("AccountResales.Business.Models.Organization", b =>
                {
                    b.HasOne("AccountResales.Business.Models.Founder", "Founder")
                        .WithMany()
                        .HasForeignKey("FounderId");
                });
        }
    }
}
