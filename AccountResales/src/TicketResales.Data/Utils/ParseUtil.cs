﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.IO;
using CsvHelper;
using System.Xml;
using System.Collections;
using AccountResales.Data.Filters.Configurations;
using System.ComponentModel;

namespace AccountResales.Data.Utils
{
    public enum ParseType
    {
        json,
        xml,
        csv
    }

    

    public static class ParseUtil
    {

        public static string GetFormatFromAcceptString(string accept)
        {
            return accept.Split('/')[1];
        }

        public static string ParseToStringByType<T>(T item, ParseType parseType)
        {
            switch (parseType)
            {
                case ParseType.json:
                    {
                        return parseToJson<T>(item);
                    }
                case ParseType.xml:
                    {
                        return parseToXml<T>(item);
                    }
                case ParseType.csv:
                    {
                        return parseToCsv<T>(item);
                    }
                default:
                    {
                        throw new FormatException("Type not found!");
                    }
            }
        }

        public static T ParseFromString<T>(string parseString, ParseType parseType)
        {
            switch (parseType)
            {
                case ParseType.json:
                    {
                        return parseFromJson<T>(parseString);
                    }
                case ParseType.xml:
                    {
                        return parseFromXml<T>(parseString);
                    }
                case ParseType.csv:
                    {
                        return parseFromCsv<T>(parseString);
                    }
                default:
                    {
                        throw new FormatException("Type not found!");
                    }
            }
        }

        private static T parseFromCsv<T>(string parseString)
        {
            using (var csv = new CsvReader(new StringReader(parseString)))
            {
                //csv.Configuration.RegisterClassMap<GameFilterMap>();
                csv.Read();

                var item = (T)csv.GetRecord(typeof(T));
                return item;
            }
        }

        private static T parseFromXml<T>(string parseString)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stringReader = new StringReader(parseString))
            {
                var items = serializer.Deserialize(stringReader);
                return (T)items;
            }
        }

        private static T parseFromJson<T>(string parseString)
        {
            var item = JsonConvert.DeserializeObject<T>(parseString);
            return item;
        }

        private static string parseToCsv<T>(T item)
        {
            using (var csv = new CsvWriter(new StringWriter()))
            {
                //csv.Configuration.RegisterClassMap<GameFilterMap>();
                var isEnumerable = item is IEnumerable;
                if (isEnumerable)
                {
                    csv.WriteRecords(item as IEnumerable);
                }
                else
                {
                    csv.WriteRecord(item);
                    csv.Serializer.Write(csv.CurrentRecord.ToArray());
                }
                return csv.Serializer.TextWriter.ToString();
            }
        }



        private static string parseToXml<T>(T item)
        {
            XmlSerializer formatter = new XmlSerializer(item.GetType());
            using (StringWriter textWriter = new StringWriter())
            {
                formatter.Serialize(textWriter, item);
                return textWriter.ToString();
            }
        }

        private static string parseToJson<T>(T item)
        {
            return JsonConvert.SerializeObject(item);
        }
    }
}
