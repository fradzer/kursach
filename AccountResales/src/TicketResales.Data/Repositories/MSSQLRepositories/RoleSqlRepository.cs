﻿using AccountResales.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountResales.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountResales.Data.Repositories.MSSQLRepositories
{
    public class RoleSqlRepository : IRoleRepository
    {
        private ResaleDbContext dbContext;

        public RoleSqlRepository(ResaleDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(Role item)
        {
            dbContext.Roles.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Role delRole = dbContext.Roles.FirstOrDefault(x => x.Id == id);
            if(delRole != null)
            {
                dbContext.Roles.Remove(delRole);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Role> GetAll()
        {
            return dbContext.Roles;
        }

        public Role GetById(int id)
        {
            return dbContext.Roles.FirstOrDefault(x => x.Id == id);
        }

        public Role GetByName(string name)
        {
            return dbContext.Roles.FirstOrDefault(x => x.Name.Equals(name));
        }

        public void Update(Role item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }
    }
}
