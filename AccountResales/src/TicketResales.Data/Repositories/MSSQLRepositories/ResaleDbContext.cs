﻿using AccountResales.Business.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AccountResales.Data.Repositories.MSSQLRepositories
{
    public class ResaleDbContext: DbContext
    {
        public ResaleDbContext(DbContextOptions<ResaleDbContext> options)
            : base(options)
        {
        }       

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Founder> Founders { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Order> Orders { get; set; }

    }
}
