﻿using AccountResales.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountResales.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountResales.Data.Repositories.MSSQLRepositories
{
    public class OrderSqlRepository : IOrderRepository
    {
        private ResaleDbContext dbContext;

        public OrderSqlRepository(ResaleDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(Order item)
        {
            dbContext.Orders.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Order delOrder = dbContext.Orders.FirstOrDefault(x => x.Id == id);
            if(delOrder != null)
            {
                dbContext.Orders.Remove(delOrder);
                dbContext.SaveChanges();
            }
        }

        public void DeleteByAccount(Account account)
        {
            Order delOrder = dbContext.Orders.FirstOrDefault(x => x.Account.Id == account.Id);
            if (delOrder != null)
            {
                dbContext.Orders.Remove(delOrder);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Order> GetAll()
        {
            return getAll();
        }

        public IEnumerable<Order> GetAllByBuyerId(int buyerId)
        {
            return getAll().Where(x => x.Buyer.Id == buyerId);
        }

        public Order GetById(int id)
        {
            return getAll().FirstOrDefault(x => x.Id == id);
        }

        public Order GetByAccount(Account account)
        {
            return getAll().FirstOrDefault(x => x.Account.Id == account.Id);
        }

        public void Update(Order item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        private IEnumerable<Order> getAll()
        {
            return dbContext.Orders.Include(x => x.Buyer).ThenInclude(x => x.Role)
                    .Include(x => x.Account).ThenInclude(x => x.Seller).ThenInclude(x => x.Role)
                    .Include(x => x.Account).ThenInclude(x => x.Game).ThenInclude(x => x.Organization).ThenInclude(x => x.Founder);
        }
    }
}
