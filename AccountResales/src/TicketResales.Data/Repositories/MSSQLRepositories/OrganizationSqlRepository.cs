﻿using AccountResales.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountResales.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountResales.Data.Repositories.MSSQLRepositories
{
    public class OrganizationSqlRepository : IOrganizationRepository
    {
        private ResaleDbContext dbContext;

        public OrganizationSqlRepository(ResaleDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(Organization item)
        {
            dbContext.Organizations.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Organization delOrganization = dbContext.Organizations.FirstOrDefault(x => x.Id == id);
            if (delOrganization != null)
            {
                dbContext.Organizations.Remove(delOrganization);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Organization> GetAll()
        {
            return dbContext.Organizations.Include(x => x.Founder);
        }

        public IEnumerable<Organization> GetAllByFounderId(int founderId)
        {
            return dbContext.Organizations.Where(x => x.Founder.Id == founderId);
        }

        public Organization GetById(int id)
        {
            return dbContext.Organizations.Include(x => x.Founder).FirstOrDefault(x => x.Id == id);
        }

        public void Update(Organization item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }
    }
}
