﻿using AccountResales.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountResales.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountResales.Data.Repositories.MSSQLRepositories
{
    public class UserSqlRepository : IUserRepository
    {
        private ResaleDbContext dbContext;

        public UserSqlRepository(ResaleDbContext context)
        {
            this.dbContext = context;
        }

        public void Create(User item)
        {
            dbContext.Users.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            User delUser = dbContext.Users.FirstOrDefault(x => x.Id == id);
            if(delUser != null)
            {
                dbContext.Users.Remove(delUser);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<User> GetAll()
        {
            return dbContext.Users.Include(x => x.Role);
        }

        public User GetById(int id)
        {
            return dbContext.Users.Include(x => x.Role).FirstOrDefault(x => x.Id == id);
        }

        public User GetUserByEmail(string email)
        {
            return dbContext.Users.Include(x => x.Role).FirstOrDefault(x => x.Email.Equals(email));
        }

        public User GetUserByLastName(string lastName)
        {
            return dbContext.Users.Include(x => x.Role).FirstOrDefault(x => x.LastName.Equals(lastName));
        }

        public void Update(User item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }
    }
}
