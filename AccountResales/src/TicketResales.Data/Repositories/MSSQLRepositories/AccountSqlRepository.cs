﻿using AccountResales.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountResales.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountResales.Data.Repositories.MSSQLRepositories
{
    public class AccountSqlRepository : IAccountRepository
    {
        private ResaleDbContext dbContext;

        public AccountSqlRepository(ResaleDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(Account item)
        {
            dbContext.Accounts.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Account delAccount = dbContext.Accounts.FirstOrDefault(x => x.Id == id);
            if(delAccount != null)
            {
                dbContext.Accounts.Remove(delAccount);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Account> GetAll()
        {
            return getAll();
        }

        public IEnumerable<Account> GetAllByGameId(int gameId)
        {
            return getAll().Where(x => x.Game.Id == gameId);
        }

        public IEnumerable<Account> GetAllByUserId(int userId)
        {
            return getAll().Where(x => x.Seller.Id == userId);
        }

        public Account GetById(int id)
        {
            return getAll().FirstOrDefault(x => x.Id == id);
        }

        public void Update(Account item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        private IEnumerable<Account> getAll()
        {
            return dbContext.Accounts.Include(x => x.Seller).ThenInclude(x => x.Role)
                    .Include(x => x.Game).ThenInclude(x => x.Organization).ThenInclude(x => x.Founder);
        }
    }
}
