﻿using AccountResales.Business.Models;
using AccountResales.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Data.Repositories.MSSQLRepositories
{
    public class GameSqlRepository : IGameRepository
    {
        ResaleDbContext dbContext;
        public GameSqlRepository(ResaleDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(Game item)
        {
           dbContext.Games.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Game delGame = dbContext.Games.FirstOrDefault(x => x.Id == id);
            if(delGame != null)
            {
                dbContext.Games.Remove(delGame);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Game> GetAll()
        {
            return dbContext.Games.Include(x => x.Organization).ThenInclude(x => x.Founder);
        }

        public Game GetById(int id)
        {
            return dbContext.Games.Include(x => x.Organization).ThenInclude(x => x.Founder).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Game> GetByOrganizationId(int organizationId)
        {
            return dbContext.Games.Where(x=> x.Organization.Id == organizationId);
        }

        public void Update(Game item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }
    }
}
