﻿using AccountResales.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountResales.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountResales.Data.Repositories.MSSQLRepositories
{
    public class FounderSqlRepository : IFounderRepository
    {
        private ResaleDbContext dbContext;

        public FounderSqlRepository(ResaleDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(Founder item)
        {
            dbContext.Founders.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Founder founder = dbContext.Founders.FirstOrDefault(x => x.Id == id);
            if(founder != null)
            {
                dbContext.Founders.Remove(founder);
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Founder> GetAll()
        {            
            return dbContext.Founders;
        }

        public Founder GetById(int id)
        {
            return dbContext.Founders.FirstOrDefault(x => x.Id == id);
        }

        public Founder GetByName(string name)
        {
            return dbContext.Founders.FirstOrDefault(x => x.Name.Equals(name));
        }

        public void Update(Founder item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }
    }
}
