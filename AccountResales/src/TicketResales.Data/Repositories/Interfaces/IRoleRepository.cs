﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AccountResales.Data.Repositories.Interfaces
{
    public interface IRoleRepository:IRepository<Role>
    {
        Role GetByName(string name);
    }
}