﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AccountResales.Data.Repositories.Interfaces
{
    public interface IFounderRepository: IRepository<Founder>
    {
        Founder GetByName(string name);
    }
}
