﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AccountResales.Data.Repositories.Interfaces
{
    public interface IOrderRepository:IRepository<Order>
    {
        IEnumerable<Order> GetAllByBuyerId(int buyerId);
        void DeleteByAccount(Account account);
        Order GetByAccount(Account account);
    }
}
