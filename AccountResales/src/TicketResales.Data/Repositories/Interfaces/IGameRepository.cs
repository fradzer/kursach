﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AccountResales.Data.Repositories.Interfaces
{
    public interface IGameRepository:IRepository<Game>
    {
        IEnumerable<Game> GetByOrganizationId(int organizationId);
    }
}