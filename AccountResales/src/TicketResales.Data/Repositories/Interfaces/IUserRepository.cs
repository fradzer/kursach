﻿using AccountResales.Business.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AccountResales.Data.Repositories.Interfaces
{
    public interface IUserRepository:IRepository<User>
    {
        User GetUserByEmail(string email);
        User GetUserByLastName(string lastName);
    }
}
