﻿using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Data.Filters.Configurations
{
    public class GameFilterMap : CsvClassMap<GameFilter>
    {
        public GameFilterMap()
        {
            NullableConverter isNullableConverterDateTime = new NullableConverter(typeof(DateTime?));

            Map(m => m.StartDate).Name("StartDate").TypeConverter(isNullableConverterDateTime);
            Map(m => m.EndDate).Name("EndDate").TypeConverter(isNullableConverterDateTime);
            Map(m => m.PartialName).Name("GameName");



            Map(m => m.FounderIds).Name("FounderIds");
            Map(m => m.OrganizationIds).Name("OrganizationIds");
        }
        
    }
}
