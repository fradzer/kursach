﻿using AccountResales.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AccountResales.Data.Filters
{
    [Serializable]
    public class GameFilter
    {
        [XmlElement]
        public DateTime? StartDate { get; set; }

        [XmlElement]
        public DateTime? EndDate { get; set; }


        [XmlElement]
        public List<int> FounderIds { get; set; }

        [XmlElement]
        public string PartialName { get; set; }

        [XmlElement]
        public List<int> OrganizationIds { get; set; }

    }
}
