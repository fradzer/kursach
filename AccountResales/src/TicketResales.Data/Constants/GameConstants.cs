﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountResales.Data.Constants
{
    public static class GameConstants
    {
        public const int CountGamesForView = 6;
    }
}
